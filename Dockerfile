FROM php:8.1.4-fpm-alpine

RUN echo "preparing environmet for kiwi application"

ARG PHPGROUP
ARG PHPUSER

ENV PHPGROUP=laravel
ENV PHPUSER=laravel

RUN echo $PHPUSER;

RUN mkdir -p /usr/share/nginx/kiwi

RUN addgroup --system ${PHPGROUP}; exit 0
RUN adduser --system -G ${PHPGROUP} -s /bin/sh -D ${PHPUSER}; exit 0

# COPY ./conf/fpm/www.conf /usr/local/etc/php-fpm.d/www.conf

RUN apk add -q mysql-client msmtp perl wget procps shadow libzip libpng libjpeg-turbo libwebp freetype icu nodejs npm
RUN set -x
RUN apk add -q --virtual build-essentials \
    icu-dev icu-libs zlib-dev g++ make automake autoconf libzip-dev \
    libpng-dev libwebp-dev libjpeg-turbo-dev freetype-dev && \
    docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp > /dev/null && \
    docker-php-ext-install gd > /dev/null && \
    docker-php-ext-install mysqli > /dev/null && \
    docker-php-ext-install pdo_mysql > /dev/null && \
    docker-php-ext-install intl > /dev/null && \
    docker-php-ext-install opcache > /dev/null && \
    docker-php-ext-install exif > /dev/null && \
    docker-php-ext-install zip > /dev/null && \
    docker-php-ext-install pcntl > /dev/null && \
    apk del build-essentials && rm -rf /usr/src/php*

RUN wget https://getcomposer.org/composer-stable.phar -O /usr/local/bin/composer && chmod +x /usr/local/bin/composer

RUN echo "Preparation done! Starting building process."

COPY composer.json /usr/share/nginx/kiwi
COPY composer.lock /usr/share/nginx/kiwi

WORKDIR /usr/share/nginx/kiwi

RUN composer install --no-autoloader

COPY . .

RUN chown -R $PHPUSER:$PHPUSER .
USER $user

RUN chown -R $PHPUSER:$PHPUSER storage bootstrap/cache
RUN chmod -R 775 storage bootstrap/cache

#RUN composer dump-autoload
#RUN php artisan key:generate
#RUN php artisan migrate --seed
#RUN php artisan cache:clear
#RUN php artisan view:clear
#RUN php artisan config:clear

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
